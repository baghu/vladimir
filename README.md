Vladimir
========

[![Build Status](https://circleci.com/gh/ashishbaghudana/vladimir/tree/develop.svg?&style=shield&circle-token=9f1e27448eadb8e550db1e0de43cb41529e2743d)](https://circleci.com/gh/ashishbaghudana/vladimir)  [![codecov.io](https://codecov.io/github/ashishbaghudana/vladimir/coverage.svg?branch=develop)](https://codecov.io/github/ashishbaghudana/vladimir?branch=develop)

Vladimir is a text summarization tool. It uses wannabe Russian references, because one of the developer's Github account name rhymes with Vladimir. Unfortunately though, we prefer Old Monk over any kind of Vodka (sorry, Putin).

## INSTALL
```bash
$ pip install -r requirements.txt

For Mac users:
$ pip install --ignore-installed -r requirements.txt
```

## TEST
```bash
$ py.test
```

## RUN
```bash
$ python vladimir.py
```
