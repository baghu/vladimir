import setuptools
from vladimir.version import Version


setuptools.setup(name='vladimir',
                 version=Version('1.0.0').number,
                 description='Text Summarization for Putin, with a hint of vodka',
                 long_description=open('README.md').read().strip(),
                 author='Package Author',
                 author_email='sharvani.raghunath@gmail.com, ashish.baghudana26@gmail.com',
                 url='',
                 py_modules=['vladimir'],
                 install_requires=[],
                 license='MIT License',
                 zip_safe=False,
                 keywords='text summarization nlp',
                 classifiers=['NLP', 'Text Summarization'])
