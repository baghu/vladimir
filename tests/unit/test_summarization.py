from vladimir.text.summarization import TextSummarization
import pkg_resources
from tests import *
from tests.helpers import *

class TestSummarization(unittest.TestCase):

    def test_text_length(self):
        summarizer = TextSummarization()
        text = 'Ms. Clinton breezed to an easy victory in Mississippi, \
        propelled by overwhelming support from black voters, and she now has \
        more than half the delegates she needs to clinch the Democratic \
        nomination at the party\'s national convention in July.'
        self.assertEqual(summarizer.__text_length__(text), 40)

    @pytest.mark.xfail
    def test_text_length_should_fail(self):
        summarizer = TextSummarization()
        text = 'Ms. Clinton breezed to an easy victory in Mississippi, \
        propelled by overwhelming support from black voters, and she now has \
        more than half the delegates she needs to clinch the Democratic \
        nomination at the party\'s national convention in July.'
        self.assertEqual(summarizer.__text_length__(text), 30)

    @pytest.mark.xfail
    def test_summarize_text_too_small(self):
        summarizer = TextSummarizer()
        text = 'Here is just one sentence. It is too small and should fail.'
        with pytest.raises(ValueError) as e:
            summarizer.summarize(text)

    def test_summarize(self):
        summarizer = TextSummarization()
        article_path = pkg_resources.resource_filename('tests.resources',
            'sanders_article.txt')
        with open(article_path) as article:
            text = article.read()
        expected_output = pkg_resources.resource_filename('tests.resources',
            'expected_sanders_article.txt')
        with open(expected_output) as article:
            expected_output = article.read()
        self.assertEqual(summarizer.summarize(text).strip(), expected_output.strip())

    def test_summarize_word_count(self):
        summarizer = TextSummarization()
        article_path = pkg_resources.resource_filename('tests.resources',
            'sanders_article.txt')
        with open(article_path) as article:
            text = article.read()
        expected_output = 'Ms. Clinton breezed to an easy victory in ' + \
        'Mississippi, propelled by overwhelming support from black voters, and ' + \
        'she now has more than half the delegates she needs to clinch the ' + \
        'Democratic nomination at the party\'s national convention in July.'
        self.assertEqual(summarizer.summarize(text, word_count=50).strip(),
            expected_output.strip())

    def test_summarize_ratio(self):
        summarizer = TextSummarization()
        article_path = pkg_resources.resource_filename('tests.resources',
            'sanders_article.txt')
        with open(article_path) as article:
            text = article.read()
        expected_output = 'Ms. Clinton breezed to an easy victory in ' + \
        'Mississippi, propelled by overwhelming support from black voters, ' + \
        'and she now has more than half the delegates she needs to clinch ' + \
        'the Democratic nomination at the party\'s national convention in ' + \
        'July. If Mr. Rubio and Mr. Kasich can\'t win in their home states, ' + \
        'the Republican primary contest appears set to become a two-person ' + \
        'race between Mr. Trump and Mr. Cruz.'
        self.assertEqual(summarizer.summarize(text, ratio=0.05).strip(),
            expected_output.strip())
