import logging
from gensim.summarization import summarize as gensim_summarize
from gensim.summarization import keywords as gensim_keywords
from gensim.utils import tokenize

class TextSummarization(object):
    """
    Automatic Text Summarization using gensim's default implementation
    """

    def summarize(self, text, ratio=None, word_count=None):
        """
        Specify either ratio or word count. If both are specified, pick up
        ratio automatically.

        If both are not specified, use ratio = 0.5 as a default value
        """
        logger = logging.getLogger(__name__)
        if ratio is None and word_count is None:
            logger.info('Ratio and Word Count both not specified')
            logger.info('Defaulting to ratio = 0.5')
            logger.info('Summarizing text of input length: {0} words'.format(self.__text_length__(text)))
            summary = gensim_summarize(text, ratio=0.5)
            logger.info('Summary text is of length: {0} words'.format(self.__text_length__(summary)))
        elif ratio:
            logger.info('Summarize with Ratio of {0}'.format(ratio))
            logger.info('Summarizing text of input length: {0} words'.format(self.__text_length__(text)))
            summary = gensim_summarize(text, ratio=ratio)
            logger.info('Summary text is of length: {0} words'.format(self.__text_length__(summary)))
        elif word_count:
            logger.info('Summarize with word count of {0}'.format(word_count))
            summary = gensim_summarize(text, word_count=word_count)
            logger.info('Summary text is of length: {0} words'.format(self.__text_length__(summary)))
        return summary.replace('\n', ' ')

    def __text_length__(self, text):
        """
        Use gensim utils tokenizer to return number of words
        """
        num_words = sum(1 for _ in tokenize(text))
        return num_words
