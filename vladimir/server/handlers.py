import json
import logging
from tornado.web import RequestHandler

logger = logging.getLogger(__name__)

class MainHandler(RequestHandler):
    def get(self):
        logger.info('Called GET RequestHandler')
        self.write("Hello, world")

class SummarizeHandler(RequestHandler):
    def initialize(self):
        from vladimir.text.summarization import TextSummarization
        self.sum_up = TextSummarization()

    def post(self):
        try:
            logger.info('Called POST SummarizeHandler')
            data = json.loads(self.request.body)
            text = data['text']
            if 'ratio' in data:
                ratio = data['ratio']
            else:
                ratio = None
            if 'word_count' in data:
                word_count = data['word_count']
            else:
                word_count = None
            summary = self.sum_up.summarize(text, ratio=ratio, word_count=word_count)
            self.write(summary)
        except:
            logger.error('Could not process the data, likely the data format is wrong')
            self.write('Error')
            raise ValueError('Input is too small')
