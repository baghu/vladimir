import time
import signal
import logging

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

from vladimir.server.handlers import MainHandler
from vladimir.server.handlers import SummarizeHandler

logger = logging.getLogger(__name__)

define("port", default=8888, help="run on the given port", type=int)

MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 3

class WebApplication(object):
    def __init__(self, handlers=[]):
        self.handlers = handlers

    def add_handler(self, route, handler):

        self.handlers.append((route, handler))

    def make_app(self):
        return tornado.web.Application(self.handlers)

def sig_handler(sig, frame):
    logger.warn('Caught signal: %s', sig)
    tornado.ioloop.IOLoop.instance().add_callback(shutdown)

def shutdown():
    logger.info('Stopping http server')
    server.stop()

    io_loop = tornado.ioloop.IOLoop.instance()

    deadline = time.time() + MAX_WAIT_SECONDS_BEFORE_SHUTDOWN

    def stop_loop():
        now = time.time()
        if now < deadline and (io_loop._callbacks or io_loop._timeouts):
            logger.info('Will shutdown in %s seconds ...', int(deadline-now))
            io_loop.add_timeout(now + 1, stop_loop)
        else:
            io_loop.stop()
            logger.info('Shutdown')

    stop_loop()

def main():
    tornado.options.parse_command_line()

    global server

    app = WebApplication()
    app.add_handler(r'/', MainHandler)
    app.add_handler(r'/summarize', SummarizeHandler)

    server = tornado.httpserver.HTTPServer(app.make_app())
    server.listen(options.port)

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    tornado.ioloop.IOLoop.instance().start()

    logger.info("Exit...")

if __name__ == '__main__':
    main()
