from vladimir.server.main import main
import daemon

import logging

FORMAT = '%(asctime)-15s %(name)s | %(levelname)s | %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

with daemon.DaemonContext():
    logger.info('Starting server...')
    main()
